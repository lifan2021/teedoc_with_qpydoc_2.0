## CFUN相关问题

CFUN4 ==> 只关射频, 不管 SIM 卡;
CFUN0 ==> 射频跟 SIM 卡都关闭 

## 注网基础相关流程

![image-20230412124120227](../media/network-basics-1.png)

## 查看IP地址

​		通过查询该API的接口dataCall.getInfo（）获取IP地址，如获取不成功，表示当前没有拨号或者拨号没有成功，可联系FAE检查。

