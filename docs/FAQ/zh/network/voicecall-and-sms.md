## voiceCall

Q：支持voiceCall功能的模组

A：请移步到wiki官网，QuecPython类库这一节查询

## SMS短信

Q：部分型号没法通过电信卡发送短信？

A：①当没有支持volte时，走的是NAS，电信不支持（运营商的问题）。

​	  ②如果有volte，三个运营商的卡都支持。

​      ③目前型号支持volte的只有EC600NCNLC、LD、LF，EC600U可以定制。

![image-20230412123809780](../media/network-voicecall-and-sms-1.png)

