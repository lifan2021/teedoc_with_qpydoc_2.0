# QuecPython-FAQ

QuecPython-FAQ 是由QuecPython团队推出的针对常见问题的总结。在线帮助我们的用户快速检索经常问到的问题，通过简单的解释获得解答。目前常见问题的种类涵盖：开发环境与工具、应用方案、软件平台、硬件相关和测试测试。

||||
|---|---|---|
|![](./media/instruction.png)|![](./media/development-environment.png)|![](./media/application-solution.png)|
|[操作指引](./guide/README.md)|[开发环境与工具相关](./toos/README.md)|[软件相关](./software/README.md)|
|![](./media/hardware-related.png)|![](./media/software-framework.png)|![](./media/test-verification.png)|
|[硬件功能相关](./hardware/README.md)|[网络功能相关](./network/README.md)|[量产和商业应用](./mp/README.md)|
